<?php include_once('common/header.php') ;?>
    <script type="text/javascript" langage="javascript">
        afficherHeure();
    </script>


    <!-- SECTION CENTRALE -->
    <section>
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-9 col-lg-10 ">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2>
                                <a href="gallery.php"> Album Photos</a>
                            </h2>
                        </div>


                        <!-- GROUPE -->
                        <div class="panel-body">
                            <p class="lead ">
                                Les photos du groupe
                            </p>

                            <!--    ------------------------CAROUSEL    ----  -->

                            <div class="col-md-12 ">
                                <hr>


                                <!-- ICI LA PHOTO  -->
                                <!-- image -->
                                <ul class="list-inline myGallery">
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="0"><img class="img-thumbnail" src="assets/images/arduino.png"><br> Caption
                                        </a>
                                    </li>
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="1"><img class="img-thumbnail" src="assets/images/expo-riot.jpg"><br> Caption
                                        </a>
                                    </li>
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="2"><img class="img-thumbnail" src="assets/images/Lenovopc.png"><br> Caption
                                        </a>
                                    </li>
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="3"><img class="img-thumbnail" src="assets/images/phone.png"><br> Caption
                                        </a>
                                    </li>
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="4"><img class="img-thumbnail" src="assets/images/expo-vogue.jpg"><br> Caption
                                        </a>
                                    </li>
                                    <li data-toggle="modal" data-target="#myModal">
                                        <a href="#myGallery" data-slide-to="5"><img class="img-thumbnail" src="assets/images/expo-lyft.jpg"><br> Caption
                                        </a>
                                    </li>
                                    <!--end of thumbnails-->
                                </ul>
                                <!-- /image -->
                                <!-- modale windows -->
                                <!--begin modal window-->
                                <div class="modal fade" id="myModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="pull-left">Ralalanantenaina mirana</div>
                                                <button type="button" class="close" data-dismiss="modal" title="Close"> <span class="glyphicon glyphicon-remove"></span></button>
                                            </div>
                                            <div class="modal-body">

                                                <!--CAROUSEL CODE GOES HERE-->
                                                <!--begin carousel-->
                                                <div id="myGallery" class="carousel slide" data-interval="false">
                                                    <div class="carousel-inner">
                                                        <div class="item active"> <img src="assets/images/arduino.png" alt="item0">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 0 description.</p>
                                                            </div>
                                                        </div>
                                                        <div class="item"> <img src="assets/images/expo-riot.jpg" alt="item1">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 1 description.</p>
                                                            </div>
                                                        </div>
                                                        <div class="item"> <img src="assets/images/Lenovopc.png" alt="item2">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 2 description.</p>
                                                            </div>
                                                        </div>
                                                        <div class="item"> <img src="assets/images/phone.png" alt="item3">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 3 description.</p>
                                                            </div>
                                                        </div>
                                                        <div class="item"> <img src="assets/images/expo-vogue.jpg" alt="item4">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 4 description.</p>
                                                            </div>
                                                        </div>
                                                        <div class="item"> <img src="assets/images/expo-lyft.jpg" alt="item5">
                                                            <div class="carousel-caption">
                                                                <h3>Heading 3</h3>
                                                                <p>Slide 5 description.</p>
                                                            </div>
                                                        </div>
                                                        <!--end carousel-inner-->
                                                    </div>
                                                    <!--Begin Previous and Next buttons-->
                                                    <a class="left carousel-control" href="#myGallery" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span></a>
                                                    <a class="right carousel-control" href="#myGallery" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span></a>
                                                    <!--end carousel-->
                                                </div>
                                                <!--/Carousel code -->

                                                <!--end modal-body-->
                                            </div>
                                            <div class="modal-footer">
                                                <div class="pull-left">
                                                    <small>Photographs by <a href="https://web.facebook.com/vivien.ralalanantenaina target="new">Mirana.facebook.com</a></small>
                                                </div>
                                                <button class="btn-sm close" type="button" data-dismiss="modal">Close</button>
                                                <!--end modal-footer-->
                                            </div>
                                            <!--end modal-content-->
                                        </div>
                                        <!--end modal-dialoge-->
                                    </div>
                                    <!--end myModal-->
                                </div>
                                <!-- /modale windows -->

                            </div>

                        </div>
                        <!-- /GROUPE -->
                    </div>
                    <!--   -------------------------------------------/body------------------------------ -->
                    <div class="panel panel-warning ">
                        <div class="panel-heading ">
                            <h3>Actualites</h3>
                        </div>
                        <div class="panel-body ">
                            <p>

                                <marquee scrollamount=5 DIRECTION="down , left ">
                                    <font color="blue " size=16>A vos PUBLICITÉS.... </font>
                                </marquee>

                            </p>
                        </div>
                    </div>
                </div>
                <!-- SECTION CENTRALE -->
                <!-- SECTION lateral -->
                <div class="col-md-3  col-lg-2 col-md-offset-0 ">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <h3>Qui suis-je ?</h3>
                        </div>
                        <div class="panel-body ">
                            <p>
                                Ralalanantenaina <br> Mirana vivien <br> 22 ans <br> ville: Antananarivo
                            </p>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading ">
                            <h2>Expériences</h2>
                        </div>
                        <div class="panel-body ">
                            <p>
                                <ul>
                                    <li>Developpement front-end: </li>
                                    <li>HTML , CSS , JavaScript ,JQuery </li>
                                    <li>Framework : Bootstrap twitter </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading ">
                            <h2>Contact</h2>
                        </div>
                        <div class="panel-body ">
                            <p>
                                <a href="tel:+261345868528 "><span class="fa fa-phone "> tel : +261345868528 </span><a>
                            </p>
                            <p>
                              <a href="mailto:miranavvs@gamil.com "><span class="fa fa-google "> email: miranavvs@gamil.com </span><a>                 
                            </p>
                            <p>
                                <a href="http://www.facebook.com/vivien.ralalanantenaina "><span class="fa fa-facebook "> Miranah vvs </span><a>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>
    <!-- SECTION CENTRALE -->




    <!--WEEL-->

    <div class="row ">
        <div class="col-md-12 col-xs-12 col-lg-10 col-lg-offset-1 ">
            <div class="page-header ">
                <h1>Parôle </h1>
            </div>
            <div class="well ">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur
                    et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
        </div>
    </div>
   <!--/WEEL-->



<?php include_once('common/footer.php') ;?>
