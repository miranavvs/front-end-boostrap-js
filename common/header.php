<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width , initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/1.png">

    <title>Mon-site</title>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/perso.css">
    <link rel="stylesheet" href="assets/css/vivien.css">
    <link rel="stylesheet" href="assets/css/font-awesome-4.6.3.min.css">
    <link rel="stylesheet" href="assets/css/background-color.css">
    <link rel="stylesheet" href="assets/css/style-footer.css">
    

    <!--FONCTION--POUR-AFFICHER-L'hEURE -->
    <script language="Javascript">
        function getDate() {
            d = new Date();
            dt = d.toLocaleDateString();
            h = d.getHours();
            min = d.getMinutes();
            s = d.getSeconds();
            tm = " " + ((h < 10) ? "0" : "") + h + ":";
            tm += ((min < 10) ? "0" : "") + min + ":";
            tm += ((s < 10) ? "0" : "") + s + " ";
            tm += " " + dt;
            document.date.mydate.value = tm;
            setTimeout("getDate()", 1000);
        }
    </script>
    <!--Fonction--date et heure fixé -->
    <script langage="javascript">
        function datetime() {
            madate = new Date();
            dat = madate.toLocaleDateString();
            time = madate.toLocaleTimeString();
            dtime = dat + "" + time;
            document.form2.matime.value = dtime;
        }
    </script>
    <!--    zoom au survol de la souri  -->
    <style>
        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }
        
        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size, 
                add browser prefixes**/
            -ms-transform: scale(2.5);
            -moz-transform: scale(2.5);
            -webkit-transform: scale(2.5);
            -o-transform: scale(2.5);
            transform: scale(2.5);
            position: relative;
            z-index: 100;
        }
        /**To keep upscaled images visible on mobile, 
                increase left & right margins a bit**/
        
        @media only screen and (max-width: 768px) {
            ul.gallery {
                margin-left: 15vw;
                margin-right: 15vw;
            }
            /**TIP: Easy escape for touch screens,
                        give gallery's parent container a cursor: pointer.**/
            .DivName {
                cursor: s-resize;
            }
        }
    </style>

    <!-- style image viewer -->
    <style>
        .myGallery li {
            width: 50%;
            margin-left: 2%;
            cursor: pointer;
        }
        /* Special Rules for md / lg screens */
        
        @media only screen and (min-width: 769px) {
            .myGallery li {
                padding: 0.25%;
                width: 22%;
                /**adjust width as desired**/
            }
        }
        /**Bootstrap overrides**/
        
        .modal-header {
            border: none
        }
        
        .modal-footer {
            text-align: left
        }
    </style>
    <style>
      .image{
         background-image:url(images/phone.png);
         width:100%;
         heogth:50%;
      }
    </style>


</head>

<body onLoad="getDate()">

    <!--time-Marché-- -->
    
    <form class="navbar-form navbar-right " name="date">
       <input class="btn btn-info disabled" name="mydate" value="">
    </form>
    <!-- ------ -   -->

    <header class="image">
        <div class="container ">
            <div class="row">
                <div class="col-md-10 ">
                    <h1>
                        <span class="fa fa-user"></span>
                        <a href="index.php">Mirana.com</a>
                        <br>
                        <small>Informatique: web , Réseau / système</small>
                    </h1>
                </div>
                <div class="col-md-2 bordureVerte">
                    <a href="images/cv-web.pdf">Telecharger mon CV</a><br>
                    <a href="">Partagez</a>
                </div>
            </div>
        </div>
    </header>

    <!-- Fixed navbar ------------------------------ NAVBAR -------------------------------------- -->

    <nav class="navbar navbar-inverse   ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
                <a class="navbar-brand" href="#"><button class="btn btn-block btn-sm btn-info">Menu</button></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="index.php"><button class="btn btn-info btn-md btn-block"><i class="fa fa-home fa-2x">home</i></button></a></li>
                    <li><a href="apropos.php"><button type="button" class="btn btn-md btn-warning btn-block "> Apropos</button></a></li>
                    <!-- Drop multimedia -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><button class="btn btn-md btn-warning btn-block">Multimedias<span class="caret"></span></button></a>
                        <ul class="dropdown-menu btn btn-default">
                            <li><a href="gallery.php">Gallery</a></li>
                            <li><a href="#">Membre 2</a></li>
                            <li><a href="#">Membre 3</a></li>
                          
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                            <li class="dropdown-header">Nav header</li>
                        </ul>
                    </li>

                    <!-- drop new -->


                    <!--Drop Membre -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <button class="btn btn-md btn-block btn-info">Les Membre <span class="caret"></span></button> </a>
                        <ul class="dropdown-menu btn btn-default">
                            <li><a href="membre.php">Membre 1</a></li>
                            <li><a href="#">Membre 2</a></li>
                            <li><a href="#">Membre 3</a></li>
                            <li><a href="#">Separated link</a></li>                           
                            <li class="dropdown-header">Nav header</li>
                        </ul>
                    </li>


                    <li><a href="#market"><button type="button" class="btn btn-sm btn-success btn-block" title="shopping"><span class="fa fa-shopping-cart"></span>Shop</button></a></li>
                    <li><a href="#market"><button type="button" class="btn btn-sm btn-success btn-block"><span class="fa fa-download "></span>telecharger</button></a></li>
                    <li><a href="#market"><button type="button" class="btn btn-sm btn-warning btn-block"><span class="fa fa-phone "></span>Contact</button></a></li>
                    
                    <li>
                        <a href="inscription.html">
                            <button type="button" class="btn btn-sm btn-info btn-block"><span class="fa fa-lock "></span>Connecter</button>
                        </a>
                    </li>

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Fixed navbar ------------------------------/NAVBAR -------------------------------------- -->

    <script type="text/javascript" langage="javascript">
        afficherHeure();
    </script>

