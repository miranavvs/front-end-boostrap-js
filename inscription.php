<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width , initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/1.png">

    <title>Mon-site</title>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/perso.css">
    <link rel="stylesheet" href="assets/css/vivien.css">
    <link rel="stylesheet" href="assets/css/font-awesome-4.6.3.min.css">
    <link rel="stylesheet" href="assets/css/background-color.css">
    <link rel="stylesheet" href="assets/css/style-footer.css">
    

    <!--FONCTION--POUR-AFFICHER-L'hEURE -->
    <script language="Javascript">
        function getDate() {
            d = new Date();
            dt = d.toLocaleDateString();
            h = d.getHours();
            min = d.getMinutes();
            s = d.getSeconds();
            tm = " " + ((h < 10) ? "0" : "") + h + ":";
            tm += ((min < 10) ? "0" : "") + min + ":";
            tm += ((s < 10) ? "0" : "") + s + " ";
            tm += " " + dt;
            document.date.mydate.value = tm;
            setTimeout("getDate()", 1000);
        }
    </script>
    <!--Fonction--date et heure fixé -->
    <script langage="javascript">
        function datetime() {
            madate = new Date();
            dat = madate.toLocaleDateString();
            time = madate.toLocaleTimeString();
            dtime = dat + "" + time;
            document.form2.matime.value = dtime;
        }
    </script>
    <!--    zoom au survol de la souri  -->
    <style>
        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }
        
        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size, 
                add browser prefixes**/
            -ms-transform: scale(2.5);
            -moz-transform: scale(2.5);
            -webkit-transform: scale(2.5);
            -o-transform: scale(2.5);
            transform: scale(2.5);
            position: relative;
            z-index: 100;
        }
        /**To keep upscaled images visible on mobile, 
                increase left & right margins a bit**/
        
        @media only screen and (max-width: 768px) {
            ul.gallery {
                margin-left: 15vw;
                margin-right: 15vw;
            }
            /**TIP: Easy escape for touch screens,
                        give gallery's parent container a cursor: pointer.**/
            .DivName {
                cursor: s-resize;
            }
        }
    </style>

    <!-- style image viewer -->
    <style>
        .myGallery li {
            width: 50%;
            margin-left: 2%;
            cursor: pointer;
        }
        /* Special Rules for md / lg screens */
        
        @media only screen and (min-width: 769px) {
            .myGallery li {
                padding: 0.25%;
                width: 22%;
                /**adjust width as desired**/
            }
        }
        /**Bootstrap overrides**/
        
        .modal-header {
            border: none
        }
        
        .modal-footer {
            text-align: left
        }
    </style>
    <style>
      .image{
         background-image:url(images/phone.png);
         width:100%;
         heogth:50%;
      }
    </style>


</head>


<body onLoad="getDate()">

    <!--time-Marché-- --------------------------------------------------- -->
    
    <form class="navbar-form navbar-right " name="date">
       <input class="btn btn-info disabled" name="mydate" value="">
    </form>
    <!-- ------ /time marche  --------------------------------------------------------- -->
     <!--  -------------------------------------------/image heaer------------ -->

    <header class="image">
        <div class="container ">
            <div class="row">
                <div class="col-md-10 ">
                    <h1>
                        <span class="fa fa-user"></span>
                        <a href="index.html">Mirana.com</a>
                        <br>
                        <small>Informatique: web , Réseau / système</small>
                    </h1>
                </div>
                <div class="col-md-2 bordureVerte">
                    <a href="images/cv-web.pdf">Telecharger mon CV</a><br>
                    <a href="">Partagez</a>
                </div>
            </div>
        </div>
    </header>
   <!--  -------------------------------------------/image heaer---------------------------------- -->
   
   <!-- Fixed navbar ------------------------------ NAVBAR -------------------------------------- -->

    <nav class="navbar navbar-inverse   ">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
               
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="index.html"><button class="btn btn-info btn-md btn-block"><i class="fa fa-home fa-2x">home</i></button></a></li>
                    <li><a href="apropos.html"><button type="button" class="btn btn-md btn-warning btn-block "> Apropos</button></a></li>
                    <!-- Drop multimedia -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><button class="btn btn-md btn-warning btn-block">Multimedias<span class="caret"></span></button></a>
                        <ul class="dropdown-menu btn btn-default">
                            <li><a href="gallery.html">Gallery</a></li>
                            <li><a href="#">Membre 2</a></li>
                            <li><a href="#">Membre 3</a></li>
                          
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                            <li class="dropdown-header">Nav header</li>
                        </ul>
                    </li>

                    <!-- drop new -->


                    <!--Drop Membre -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <button class="btn btn-md btn-block btn-info">Les Membre <span class="caret"></span></button> </a>
                        <ul class="dropdown-menu btn btn-default">
                            <li><a href="tableau.html">Membre 1</a></li>
                            <li><a href="#">Membre 2</a></li>
                            <li><a href="#">Membre 3</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                            <li class="dropdown-header">Nav header</li>
                        </ul>
                    </li>

                    <!--Drop organisation -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><button class="btn btn-block btn-md btn-block btn-success">Organisation <span class="caret"></span></button></a>
                        <ul class="dropdown-menu btn btn-default ">
                            <li><a href="#">Membre 1</a></li>
                            <li><a href="#">Membre 2</a></li>
                            <li><a href="#">Membre 3</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                            <li class="dropdown-header">Nav header</li>
                        </ul>
                    </li>

                    <li><a href="#market"><button type="button" class="btn btn-md btn-success btn-block" title="shopping"><span class="fa fa-shopping-cart"></span>Shop</button></a></li>
                    <li><a href="#market"><button type="button" class="btn btn-md btn-success btn-block"><span class="fa fa-download "></span>telecharger</button></a></li>
                    <li><a href="#market"><button type="button" class="btn btn-md btn-warning btn-block"><span class="fa fa-phone "></span>Contact</button></a></li>
                    
                    <li>
                        <a href="inscription.html">
                            <button type="button" class="btn btn-md btn-info btn-block"><span class="fa fa-lock "></span>Connecter</button>
                        </a>
                    </li>

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>

    <script type="text/javascript" langage="javascript">
        afficherHeure();
    </script>
<!-- ------------------------------------/NAVbar ------------------------------------------->  






    <!-- SECTION CENTRALE -->


    <section>
    <div class="container-fluid ">
        <div class="row">

    <!--  -------------------------------BODY CENTRALE --------------------  -->
            <div class="col-md-9 col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>	<span class="label label-default  " >Enrégistrement</span></h2>
                    </div>
                    <div class="panel-body">

                        <div class="col-md-10 ">
                            <p class="lead ">
                                Inscription sur Mirana.com
                            </p>
                        </div>

                <div class="col-md-12  ">
                           <!-- ----  FORM INSCRIPTION -------------------------------------- -->
                    <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 col-lg-offset-1  ">
                              <form action="" method="post" id="fileForm" role="form">
                                <fieldset><legend class="text-center">Valid information is required to register. <span class="req"><small> required *</small></span></legend>
                          <p><b>I have Account  <a href="login.html">connect now</a><b> </p>
                    
                                <div class="form-group">
                                <label for="phonenumber"><span class="req">* </span> Phone Number: </label>
                                        <input required type="text" name="phonenumber" id="phone" class="form-control phone" maxlength="28" onkeyup="validatephone(this);" placeholder="not used for marketing"/> 
                                </div>
                    
                                <div class="form-group"> 	 
                                    <label for="firstname"><span class="req">* </span> First name: </label>
                                        <input class="form-control" type="text" name="firstname" id = "txt" onkeyup = "Validate(this)" required /> 
                                            <div id="errFirst"></div>    
                                </div>
                    
                                <div class="form-group">
                                    <label for="lastname"><span class="req">* </span> Last name: </label> 
                                        <input class="form-control" type="text" name="lastname" id = "txt" onkeyup = "Validate(this)" placeholder="hyphen or single quote OK" required />  
                                            <div id="errLast"></div>
                                </div>

                                <div class="form-group">
                                    <label for="sexe"><span class="req">* </span> Sexe: </label> 
                                        <select name="sexe" class="select">
                                               <option value="">sexe</option>
                                               <option value="homme">homme</option>
                                               <option value="femme">femme</option>
                                            </select>   
                                        <div id="errLast"></div>
                                </div>
                    
                                <div class="form-group">
                                    <label for="email"><span class="req">* </span> Email Address: </label> 
                                        <input class="form-control" required type="text" name="email" id = "email"  onchange="email_validate(this.value);" />   
                                            <div class="status" id="status"></div>
                                </div>
                    
                                <div class="form-group">
                                    <label for="username"><span class="req">* </span> User name:  <small>This will be your login user name</small> </label> 
                                        <input class="form-control" type="text" name="username" id = "txt" onkeyup = "Validate(this)" placeholder="minimum 6 letters" required />  
                                            <div id="errLast"></div>
                                </div>
                    
                                <div class="form-group">
                                    <label for="password"><span class="req">* </span> Password: </label>
                                        <input required name="password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass1" /> </p>
                    
                                    <label for="password"><span class="req">* </span> Password Confirm: </label>
                                        <input required name="password" type="password" class="form-control inputpass" minlength="4" maxlength="16" placeholder="Enter again to validate"  id="pass2" onkeyup="checkPass(); return false;" />
                                            <span id="confirmMessage" class="confirmMessage"></span>
                                </div>
                    
                                <div class="form-group">
                                
                                    <?php //$date_entered = date('m/d/Y H:i:s'); ?>
                                    <input type="hidden" value="<?php //echo $date_entered; ?>" name="dateregistered">
                                    <input type="hidden" value="0" name="activate" />
                                    <hr>
                    
                                    <input type="checkbox" required name="terms" onchange="this.setCustomValidity(validity.valueMissing ? 'Please indicate that you accept the Terms and Conditions' : '');" id="field_terms">   <label for="terms">I agree with the <a href="terms.php" title="You may read our terms and conditions by clicking on this link">terms and conditions</a> for Registration.</label><span class="req">* </span>
                                </div>
                    
                                <div class="form-group">
                                    <input class="btn btn-success" type="submit" name="submit_reg" value="Register">
                                </div>
                                          <h5>You will receive an email to complete the registration and validation process. </h5>
                                          <h5>Be sure to check your spam folders. </h5>
                     
                    
                                </fieldset>
                              </form><!-- ends register form -->
                    
                               <script type="text/javascript">
                                  document.getElementById("field_terms").setCustomValidity("Please indicate that you accept the Terms and Conditions");
                               </script>
                            </div><!-- ends col-6 -->                                
                        </div>
                    </div>
                 </div>
              </div>

                <div class="panel panel-warning ">
                    <div class="panel-heading ">
                     <h3> <span class="label label-default">Actualités</span> </h3>
                    </div>
                    <div class="panel-body ">
                        <p>

                            <marquee scrollamount=5 DIRECTION="down , left ">
                                <font color="blue " size=16>A vos PUBLICITÉS.... </font>
                            </marquee>

                        </p>
                    </div>
                </div>
            </div>
            <!--   ------------------------------------- /BODY CENTRALE     --------------------- -->
            <!--    ------------------------------------/ MAVIGATEUR A COTE --------------------- -->

            <div class="col-md-3 col-md-offset-0 col-lg-2 ">
                <div class="panel panel-info ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Qui suis-je ?</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            Ralalanantenaina <br> Mirana vivien <br> 22 ans <br> ville: Antananarivo
                        </p>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Expériences</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            <ul>
                                <li>Developpement front-end: </li>
                                <li>HTML , CSS , JavaScript ,JQuery </li>
                                <li>Framework : Bootstrap twitter </li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Contactes</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            <a href="tel:+261345868528 "><span class="fa fa-phone "> tel : +261345868528 </span><a>
                            </p>
                            <p>
                              <a href="mailto:miranavvs@gamil.com "><span class="fa fa-google "> email: miranavvs@gamil.com </span><a>                 
                            </p>
                            <p>
                                <a href="http://www.facebook.com/vivien.ralalanantenaina "><span class="fa fa-facebook "> Miranah vvs </span><a>
                            </p>

                       </div>
                    </div>
                </div>
                <!--  -----------------------------------/NAVIGATEYR A COTE -------------------------  -->
            </div>
        </div>
    </section>


    <!--WEEL-->

    <div class="row ">
        <div class="col-md-12 col-xs-12 col-lg-10 col-lg-offset-1 ">
            <div class="page-header ">
                <h1>Parôle </h1>
            </div>
            <div class="well ">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur
                    et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
        </div>
    </div>
    <!-- /WEEL -->



    <!-- ---------------------------------------FOOTER---------------------------------------- -->
     </footer>
        <div class="footer " id="footer ">
            <div class="container ">
                <div class="row ">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 ">
                        <h3> Actualité </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            </ul>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> News </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> Contacts </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> Liens </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                        <h3> News </h3>
                        <ul>
                            <li>
                                <div class="input-append newsletter-box text-center ">
                                    <input type="text " class="full text-center " placeholder="Email ">
                                    <button class="btn bg-gray " type="button "> Gasy-gasy <i class="fa fa-long-arrow-right "> </i> </button>
                                </div>
                            </li>
                        </ul>
                        <ul class="social ">
                            <li>
                                <a href="# "> <i class=" fa fa-facebook ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-twitter ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-google-plus ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-pinterest ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-youtube ">   </i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </div>
        <!-- ---------------------------------------------footer--------------------------- -->

        <!-- --------------------------------------------payment footer ------------------- -->
        <div class="footer-bottom ">
            <div class="container ">
                <p class="pull-left "> Copyright ©&nbsp;
                    <?php $date=date('Y'); echo $date; ?> Footer All right reserved. </p>
                <div class="pull-right ">
                    <ul class="nav nav-pills payments ">

                        <li><a href="# "><i class="fa fa-cc-visa "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-mastercard "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-amex "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-paypal "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-visa "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-mastercard "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-amex "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-paypal "></i></a></li>
                        <li><a href="# "><i class="fa fa-cc-paypal "></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- --------------------------------------------payment footer ------------------- -->
     </footer>
    <!-- ---------------------------------------FOOTER---------------------------------------- -->



        <!-- Bootstrap core javascript doit se place ici pour executer rapidement -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js ">
        </script>
        <script>
            window.jQuery || document.write('<script src="assets/js/jquery-3.2.1.min.js "><\/script>')
        </script>
        <script src="assets/js/bootstrap.min.js "></script>
        <scrip src="assets/js/DATE-HEURE.js "></scrip>
        <script src="assets/js/npm.js "></script>
        <scrip ser="assets/js/IMAGE.JS "></scrip>


        <!-- jQuery image viewer -->
        <script>
            $(document).ready(function() {
                $('.myGallery li img').on('click', function() {
                    var src = $(this).attr('src');
                    var img = '<img src="' + src + '" class="img-responsive"/>';
                    var title = $(this).attr('title');
                    $('#myModal').modal();
                    $('#myModal').on('shown.bs.modal', function() {
                        $('#myModal .modal-body').html(img);
                        $('#myModal .modal-footer').html(title);
                    });
                    $('#myModal').on('hidden.bs.modal', function() {
                        $('#myModal .modal-body').html('');
                        $('#myModal .modal-footer').html('');
                    });

                });
            })
        </script>

        <!-- pour mobil -->
        <!-- --------------------------- -->
        <script>
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <!-- --------------------------- -->
        <script>
            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {



                var msViewportStyle = document.createElement("style")
                msViewportStyle.appendChild(
                    document.createTextNode(
                        "@-ms-viewport{width:auto!important}"
                    )
                )
                document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
            }
        </script>
 		<script>

        </script>
    </div>
</body>
</html>
