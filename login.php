<?php include_once('common/header.php'); ?>

    <script type="text/javascript" langage="javascript">
        afficherHeure();
    </script>

    <!-- SECTION CENTRALE -->
    <div class="container-fluid ">
        <div class="row">
            <!--  -------------------------------BODY CENTRALE --------------------  -->
            <div class="col-md-9 col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>	<span class="label label-default  " >Sign in</span></h2>
                    </div>
                    <div class="panel-body">

                        <div class="col-md-10 ">
                            <p class="lead ">
                                Developpement front-end
                            </p>
                        </div>
                        <!--form login-->

<div class="container">
    <div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Please sign in</h3>
        </div>
          <div class="panel-body">
     
                    <fieldset>
                <div class="form-group">
                  <input class="form-control" placeholder="E-mail" name="identity" type="text">
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Password" name="password" type="password" value="">
              </div>
              <div class="checkbox">
                  <label>
                    <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                  </label>
                </div>
              <input class="btn btn-lg btn-default btn-block" type="submit" value="Login">
              <p style="margin-top: 15px;" class="text-center">
               
                <a class="" href="inscription.html">Don't have an account?</a>
              </p>
            </fieldset>
              </form>
          </div>
      </div>
    </div>
  </div>
</div>
                        
                        <!--form     -->
                    </div>
                </div>

                <div class="panel panel-warning ">
                    <div class="panel-heading ">
                     <h3> <span class="label label-default">Actualités</span> </h3>
                    </div>
                    <div class="panel-body ">
                        <p>

                            <marquee scrollamount=5 DIRECTION="down , left ">
                                <font color="blue " size=16>A vos PUBLICITÉS.... </font>
                            </marquee>

                        </p>
                    </div>
                </div>
            </div>
            <!--   --------------------------/BODY CENTRALE ------------------------ -->
          <!--    ---------------------------/ MAVIGATEUR A COTE --------------------- -->

            <div class="col-md-3 col-md-offset-0 col-lg-2 ">
                <div class="panel panel-info ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Qui suis-je ?</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            Ralalanantenaina <br> Mirana vivien <br> 22 ans <br> ville: Antananarivo
                        </p>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Expériences</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            <ul>
                                <li>Developpement front-end: </li>
                                <li>HTML , CSS , JavaScript ,JQuery </li>
                                <li>Framework : Bootstrap twitter </li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading ">
                        <h3><span class="label label-default">Contactes</span></h3>
                    </div>
                    <div class="panel-body ">
                        <p>
                            <a href="tel:+261345868528 "><span class="fa fa-phone "> tel : +261345868528 </span><a>
                            </p>
                            <p>
                              <a href="mailto:miranavvs@gamil.com "><span class="fa fa-google "> email: miranavvs@gamil.com </span><a>                 
                            </p>
                            <p>
                                <a href="http://www.facebook.com/vivien.ralalanantenaina "><span class="fa fa-facebook "> Miranah vvs </span><a>
                            </p>

                       </div>
                    </div>
                </div>
          <!--  -----------------------------/ NAVIGATEYR A COTE -------------------------  -->
        </div>
    </div>
    </section>
    <!-- /SECTION CENTRALE -->


    <!--WEEL-->

    <div class="row ">
        <div class="col-md-12 col-xs-12 col-lg-10 col-lg-offset-1 ">
            <div class="page-header ">
                <h1>Parôle </h1>
            </div>
            <div class="well ">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur
                    et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
        </div>
    </div>
    <!-- /WEEL -->



<?php include_once('common/footer.php'); ?>
